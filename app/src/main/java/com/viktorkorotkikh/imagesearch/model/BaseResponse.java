package com.viktorkorotkikh.imagesearch.model;

/**
 * Created by korotkikh.viktor@gmail.com on 18.03.2015.
 */

/**
 * {
 * "responseData" : {
 * "results" : [],
 * "cursor" : {}
 * },
 * "responseDetails" : null | string-on-error,
 * "responseStatus" : 200 | error-code
 * }
 *
 * @param <T> type of results
 */
public class BaseResponse<T> {
    private static final int RESPONSE_STATUS_SUCCCESS = 200;

    private ResponseData<T> responseData;

    private String responseDetails; // null | string-on-error,

    private int responseStatus; // 200 | error-code

    public ResponseData<T> getResponseData() {
        return responseData;
    }

    public void setResponseData(ResponseData<T> responseData) {
        this.responseData = responseData;
    }

    public String getResponseDetails() {
        return responseDetails;
    }

    public void setResponseDetails(String responseDetails) {
        this.responseDetails = responseDetails;
    }

    public int getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(int responseStatus) {
        this.responseStatus = responseStatus;
    }

    public boolean isSuccess() {
        return RESPONSE_STATUS_SUCCCESS == responseStatus;
    }

    @Override
    public String toString() {
        return "BaseResponse : responseStatus = " + responseStatus;
    }
}
