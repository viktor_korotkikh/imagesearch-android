package com.viktorkorotkikh.imagesearch.model;

import java.util.List;

/**
 * Created by korotkikh.viktor@gmail.com on 18.03.2015.
 */

/**
 * "responseData" : {
 * "results" : [],
 * "cursor" : {}
 * }
 *
 * @param <T> type of results
 */
public class ResponseData<T> {

    private List<T> results;

    public List<T> getResults() {
        return results;
    }

    public void setResults(List<T> results) {
        this.results = results;
    }

    @Override
    public String toString() {
        return "ResponseData : " + results.size() + " : " + results.toString();
    }
}
