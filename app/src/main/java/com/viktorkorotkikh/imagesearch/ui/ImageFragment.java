package com.viktorkorotkikh.imagesearch.ui;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.squareup.picasso.Picasso;
import com.viktorkorotkikh.imagesearch.R;
import com.viktorkorotkikh.imagesearch.model.Image;

import uk.co.senab.photoview.PhotoView;
import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by korotkikh.viktor@gmail.com on 19.03.2015.
 */
public class ImageFragment extends DialogFragment {
    public static final String TAG = ImageFragment.class.getName();
    private static final String STATE_IMAGE = "state_image";

    private Image image;
    private PhotoView fullImageView;
    private PhotoViewAttacher photoViewAttacher;

    public static ImageFragment newInstance(Image image) {
        ImageFragment fragment = new ImageFragment(image);
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public ImageFragment() {
    }

    public ImageFragment(Image image) {
        this.image = image;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(STATE_IMAGE)) {
                image = (Image) savedInstanceState.getSerializable(STATE_IMAGE);
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        // safety check
        if (getDialog() == null) {
            return;
        }

        // set the animations to use on showing and hiding the dialog
        getDialog().getWindow().setWindowAnimations(
                R.style.dialog_animation_fade);
        // alternative way of doing it
        //getDialog().getWindow().getAttributes().
        //    windowAnimations = R.style.dialog_animation_fade;

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (getActivity() != null) {
            final Dialog dialog = new Dialog(getActivity());
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.fragment_image);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
            layoutParams.copyFrom(dialog.getWindow().getAttributes());
            layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setAttributes(layoutParams);
            fullImageView = (PhotoView) dialog.findViewById(R.id.full_imageView);
            if (image != null) {
                Picasso.with(getActivity())
                        .load(image.getUrl())
                        .placeholder(android.R.color.transparent)
                        .error(R.mipmap.ic_launcher)
                        .fit()
                        .centerInside()
                        .into(fullImageView);
            } else {
                dismiss();
            }
            photoViewAttacher = new PhotoViewAttacher(fullImageView);
            photoViewAttacher.setOnViewTapListener(new PhotoViewAttacher.OnViewTapListener() {
                @Override
                public void onViewTap(View view, float x, float y) {
                    dismiss();
                }
            });
            return dialog;
        } else {
            return null;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (image != null) {
            outState.putSerializable(STATE_IMAGE, image);
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance())
            getDialog().setDismissMessage(null);
        super.onDestroyView();
    }

}
