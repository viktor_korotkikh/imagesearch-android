package com.viktorkorotkikh.imagesearch.ui;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;
import com.viktorkorotkikh.imagesearch.R;
import com.viktorkorotkikh.imagesearch.model.Image;
import com.viktorkorotkikh.imagesearch.util.LogUtils;

import java.util.List;

/**
 * Created by korotkikh.viktor@gmail.com on 17.03.2015.
 */
public class RecyclerImagesAdapter extends RecyclerView.Adapter<RecyclerImagesAdapter.ViewHolder> {
    private static final String TAG = RecyclerImagesAdapter.class.getSimpleName();

    private OnItemClickListener onItemClickListener;
    private List<Image> images;

    public RecyclerImagesAdapter(List<Image> images){
        this.images = images;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.image_item_view, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        LogUtils.d(TAG, "onBindViewHolder: " + i);
        Image image = images.get(i);
        LogUtils.d(TAG, "onBindViewHolder: image = " + image.toString());
        Picasso.with(viewHolder.itemView.getContext())
                .load(image.getUrl())
                .placeholder(android.R.color.transparent)
                .error(R.mipmap.ic_launcher)
                .fit()
                .centerCrop()
                .into(viewHolder.imageView);
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    public List<Image> getImages() {
        return images;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        abstract void onItemClick(View view, int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        SquareImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = (SquareImageView) itemView.findViewById(R.id.imageView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(onItemClickListener != null) {
                        onItemClickListener.onItemClick(v, getPosition());
                    }
                }
            });
        }
    }
}
