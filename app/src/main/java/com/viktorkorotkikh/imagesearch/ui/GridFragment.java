package com.viktorkorotkikh.imagesearch.ui;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.viktorkorotkikh.imagesearch.R;
import com.viktorkorotkikh.imagesearch.model.BaseResponse;
import com.viktorkorotkikh.imagesearch.model.Image;
import com.viktorkorotkikh.imagesearch.network.RestClient;
import com.viktorkorotkikh.imagesearch.util.LogUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by korotkikh.viktor@gmail.com on 18.03.2015.
 */
public class GridFragment extends Fragment implements Callback<BaseResponse<Image>>,
        SearchView.OnQueryTextListener,
        RecyclerImagesAdapter.OnItemClickListener {
    private static final String TAG = GridFragment.class.getSimpleName();

    private static final int SPAN_COUNT_LANDSCAPE = 3;
    private static final int SPAN_COUNT_PORTRAIT = 2;

    private View emptyView;
    private RecyclerView gridView;
    private RecyclerImagesAdapter imagesAdapter;
    private GridLayoutManager gridLayoutManager;

    private String query = "pugs";
    private boolean isLoading = false;

    public GridFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        emptyView = rootView.findViewById(R.id.empty_view);
        gridView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        gridView.setHasFixedSize(true);
        gridLayoutManager = new GridLayoutManager(getActivity(), getSpanCount());
        gridView.setLayoutManager(gridLayoutManager);
//            gridView.setItemAnimator(new DefaultItemAnimator());
//            gridView.scrollToPosition(scrollPosition);

        initEmptyAdapter();

        return rootView;
    }

//    @Override
//    public void onConfigurationChanged(Configuration newConfig) {
//        super.onConfigurationChanged(newConfig);
//        // Checks the orientation of the screen
//        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
//            LogUtils.d(TAG, "onConfigurationChanged: landscape");
//            gridLayoutManager.setSpanCount(SPAN_COUNT_LANDSCAPE);
//        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
//            LogUtils.d(TAG, "onConfigurationChanged: portrait");
//            gridLayoutManager.setSpanCount(SPAN_COUNT_PORTRAIT);
//        }
//    }

    /**
     * Callback on success responce to server.
     */
    @Override
    public void success(BaseResponse<Image> baseResponse, Response response) {
        if (baseResponse.isSuccess()) {
            LogUtils.d(TAG, "search : getResponseData = " + String.valueOf(baseResponse.getResponseData()));
            showGrid();
            addImages(baseResponse.getResponseData().getResults());
        } else {
            LogUtils.d(TAG, "search : ResponseStatus = " + String.valueOf(baseResponse.getResponseStatus()));
            LogUtils.d(TAG, "search : ResponseDetails = " + String.valueOf(baseResponse.getResponseDetails()));
        }
        isLoading = false;
    }

    /**
     * Callback on failure response from server.
     */
    @Override
    public void failure(RetrofitError error) {
        LogUtils.d(TAG, "search : failure : " + error.toString());
        Toast.makeText(getActivity(), "Failure response from server", Toast.LENGTH_SHORT).show();
        isLoading = false;
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        LogUtils.d(TAG, "onQueryTextSubmit: " + String.valueOf(s));
        searchImage(s);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        LogUtils.d(TAG, "onQueryTextChange: " + String.valueOf(s));
        return false;
    }

    @Override
    public void onItemClick(View view, int position) {
        LogUtils.d(TAG, "onItemClick: " + String.valueOf(position));

        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager != null) {
            ImageFragment imageFragment = ImageFragment.newInstance(imagesAdapter.getImages().get(position));
            imageFragment.show(fragmentManager, ImageFragment.TAG);
        }
    }

    private void setupAdapter(List<Image> data) {
        imagesAdapter = new RecyclerImagesAdapter(data);
        imagesAdapter.setOnItemClickListener(this);
        gridView.setAdapter(imagesAdapter);
        gridView.setOnScrollListener(onScroll);
    }

    private void initEmptyAdapter() {
        setupAdapter(initDataset());
    }

    private void addImages(List<Image> newImages) {
        LogUtils.d(TAG, "addImages : " + newImages.size());
        imagesAdapter.getImages().addAll(newImages);
        imagesAdapter.notifyDataSetChanged();
    }

    /**
     * Generates icons for RecyclerView's adapter. This data would usually come
     * from a local content provider or remote server.
     */
    private List<Image> initDataset() {
        LogUtils.d(TAG, "initDataset");
        List<Image> dataList = new ArrayList<Image>();
        //        Image image = new Image();
        //        image.setUrl("http://www.themost10.com/wp-content/uploads/2012/11/French-bulldog-By-Sonya-Yu-3.jpg?5b7486");
        //        for (int i = 1; i < 8; i++) {
        //            dataList.add(image);
        //        }
        return dataList;
    }

    private void searchImage(String query) {
        LogUtils.d(TAG, "searchImage : query = " + query);
        this.query = query;
        initEmptyAdapter();
        loadImages(query, 0);
    }

    /**
     * Search and download images.
     *
     * @param query  String query, that is passed into the searcher.
     * @param offset Start index of the first search result.
     */
    private void loadImages(String query, int offset) {
        isLoading = true;
        RestClient.getApiInstance().search(query, offset, this);
    }

    /**
     * Only for tests!
     */
    private void loadPugsImages() {
        loadImages("pugs", 0);
    }

    private void getMoreImagesIfNeeded() {
        int lastitem = ((GridLayoutManager) gridView.getLayoutManager()).findLastVisibleItemPosition();
        int totalItemCount = imagesAdapter.getItemCount();
        boolean shouldLoadMore = lastitem > 0 && lastitem > totalItemCount - 9;
        if (shouldLoadMore) LogUtils.d(TAG, "shouldLoadMore = " + String.valueOf(shouldLoadMore)
                + " : lastitem = " + lastitem
                + " : totalItemCount = " + totalItemCount);
        if (shouldLoadMore && !isLoading) {
            LogUtils.d(TAG, "download more!");
            loadImages(query, totalItemCount);
        }
    }

    private void showGrid() {
        gridView.setVisibility(View.VISIBLE);
        emptyView.setVisibility(View.GONE);
    }

    private int getSpanCount() {
//            return getResources().getInteger(R.integer.grid_span_count);
        return isLandscape() ? SPAN_COUNT_LANDSCAPE : SPAN_COUNT_PORTRAIT;
    }

    private boolean isLandscape() {
        return getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
    }

    private RecyclerView.OnScrollListener onScroll = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            //Load more images
            getMoreImagesIfNeeded();
        }
    };
}
