package com.viktorkorotkikh.imagesearch.util;

import android.util.Log;

import com.viktorkorotkikh.imagesearch.BuildConfig;

/**
 * Created by korotkikh.viktor@gmail.com on 19.03.2015.
 */
public class LogUtils {

    private static boolean SHOW_LOG = Boolean.FALSE;

    private static boolean ERROR_LEVEL = Boolean.TRUE;
    private static boolean WARNING_LEVEL = Boolean.TRUE;
    private static boolean INFO_LEVEL = Boolean.TRUE;
    private static boolean DEBUG_LEVEL = Boolean.TRUE;
    private static boolean VERBOSE_LEVEL = Boolean.TRUE;

    private static boolean BUILDCONFIG_DEBUG = BuildConfig.DEBUG;


    /**
     * Sends an INFO log message.
     *
     * @param msg The message you would like logged.
     */
    public static void i(String tag, String msg) {
        if (SHOW_LOG && BUILDCONFIG_DEBUG && INFO_LEVEL) {
            Log.i(tag, msg);
        }
    }

    /**
     * Sends an INFO log message and log the exception.
     *
     * @param msg The message you would like logged.
     * @param tr  An exception to log.
     */
    public static void i(String tag, String msg, Throwable tr) {
        if (SHOW_LOG && BUILDCONFIG_DEBUG && INFO_LEVEL) {
            Log.i(tag, msg, tr);
        }
    }

    /**
     * Sends an DEBUG log message.
     *
     * @param msg The message you would like logged.
     */
    public static void d(String tag, String msg) {
        if (SHOW_LOG && BUILDCONFIG_DEBUG && DEBUG_LEVEL) {
            Log.d(tag, msg);
        }
    }

    /**
     * Sends an DEBUG log message and log the exception.
     *
     * @param msg The message you would like logged.
     * @param tr  An exception to log.
     */
    public static void d(String tag, String msg, Throwable tr) {
        if (SHOW_LOG && BUILDCONFIG_DEBUG && DEBUG_LEVEL) {
            Log.d(tag, msg, tr);
        }
    }

    /**
     * Sends an WARNING log message.
     *
     * @param msg The message you would like logged.
     */
    public static void w(String tag, String msg) {
        if (SHOW_LOG && BUILDCONFIG_DEBUG && WARNING_LEVEL) {
            Log.w(tag, msg);
        }
    }

    /**
     * Sends an WARNING log message and log the exception.
     *
     * @param msg The message you would like logged.
     * @param tr  An exception to log.
     */
    public static void w(String tag, String msg, Throwable tr) {
        if (SHOW_LOG && BUILDCONFIG_DEBUG && WARNING_LEVEL) {
            Log.w(tag, msg, tr);
        }
    }

    /**
     * Sends an ERROR log message.
     *
     * @param msg The message you would like logged.
     */
    public static void e(String tag, String msg) {
        if (SHOW_LOG && BUILDCONFIG_DEBUG && ERROR_LEVEL) {
            Log.e(tag, msg);
        }
    }

    /**
     * Sends an ERROR log message and log the exception.
     *
     * @param msg The message you would like logged.
     * @param tr  An exception to log.
     */
    public static void e(String tag, String msg, Throwable tr) {
        if (SHOW_LOG && BUILDCONFIG_DEBUG && ERROR_LEVEL) {
            Log.e(tag, msg, tr);
        }
    }

    /**
     * Sends an VERBOSE log message.
     *
     * @param msg The message you would like logged.
     */
    public static void v(String tag, String msg) {
        if (SHOW_LOG && BUILDCONFIG_DEBUG && VERBOSE_LEVEL) {
            Log.v(tag, msg);
        }
    }

    /**
     * Sends an VERBOSE log message and log the exception.
     *
     * @param msg The message you would like logged.
     * @param tr  An exception to log.
     */
    public static void v(String tag, String msg, Throwable tr) {
        if (SHOW_LOG && BUILDCONFIG_DEBUG && VERBOSE_LEVEL) {
            Log.v(tag, msg, tr);
        }
    }
}
