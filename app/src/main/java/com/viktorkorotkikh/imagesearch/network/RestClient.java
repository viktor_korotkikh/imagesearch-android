package com.viktorkorotkikh.imagesearch.network;

import retrofit.RestAdapter;

/**
 * Created by korotkikh.viktor@gmail.com on 18.03.2015.
 */
public class RestClient {
    private static final String TAG = RestClient.class.getSimpleName();

    private RestAdapter restAdapter;

    private RestClient() {
        restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.NONE)
                .setEndpoint(ImageSearchApi.BASE_URL)
                .build();
    }

    private ImageSearchApi createApi() {
        return restAdapter.create(ImageSearchApi.class);
    }

    /**
     * Holder is loaded on the first execution of getApiInstance()
     * or the first access to RestClientHolder.INSTANCE, not before.
     */
    private static class RestClientHolder {
        private static final ImageSearchApi INSTANCE = new RestClient().createApi();
    }

    public static ImageSearchApi getApiInstance() {
        return RestClientHolder.INSTANCE;
    }
}
