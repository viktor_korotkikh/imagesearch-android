package com.viktorkorotkikh.imagesearch.network;

import com.viktorkorotkikh.imagesearch.model.BaseResponse;
import com.viktorkorotkikh.imagesearch.model.Image;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by korotkikh.viktor@gmail.com on 18.03.2015.
 */
public interface ImageSearchApi {
    static final String BASE_URL = "https://ajax.googleapis.com/ajax/services/search";

    /**
     * Image Search request.
     * The only valid protocol version number at this point in time is 1.0.
     * Argument "rsz" supplies an integer from 1–8 indicating the number of results to return per page.
     *
     * @param query    This argument supplies the query, or search expression, that is passed into the searcher.
     * @param start    This argument supplies the start index of the first search result.
     * @param callback
     * @see "https://developers.google.com/image-search/v1/jsondevguide"
     */
    @GET("/images?v=1.0&rsz=8&")
    public void search(
            @Query("q") String query,
            @Query("start") int start,
            Callback<BaseResponse<Image>> callback);
}
